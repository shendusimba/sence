import config from "./config"
import { $showAction } from './normal'
/**
 * 提示框
 * 用法  $msg('登录成功',()=>{ 跳转页面啥的 })
 * @param {Object} str
 * @param {Object} fn
 */
export function $msg(str, fn, duration = config.MSG_TIP_DURATION) {
	uni.showToast({
		title: str,
		icon: 'none',
		duration: duration,
		success() {
			//给一个等待时间，避免提示信息没来得及看就跳转
			if (fn) setTimeout(fn, duration)
		}
	})
	return true
}

/**
 * 确认框，点确认才会往下走
 * 用法：  await $confirm('确定退出？')
 * 		  //继续做事，如果点了取消就不会走下来了
 * @param {String} title
 */
export function $confirm(title, content) {
	return new Promise((resolve, reject) => {
		uni.showModal({
			title,
			content,
			success: ({ confirm }) => confirm && resolve() || reject()
		})
	})
}

export function $confirmF(title, content) {
	return new Promise((resolve, reject) => {
		uni.showModal({
			title,
			content,
			showCancel: false,
			success: ({ confirm }) => confirm && resolve() || reject()
		})
	})
}

/**
 * 确认
 */
export function $mc(title) {
	return $showAction([title])
}
