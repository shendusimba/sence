let localCfg = {
    // 静态文件前缀(如阿里云CDN)
    STATIC_URL: "",
    // ajax路径前缀
    BASE_API: '',
    //提示信息驻留时间(毫秒)
    MSG_TIP_DURATION: 2000,
    //上传文件API
    UPLOAD_URL: '',
    //tabbar页面路径数组，防止go2时无法跳转
    TABBAR_PAGE_LIST: ['/pages/tabbar/index/index'],
    // 首页
    HOME_PAGE_PATH: "/pages/main/tabbar/index/index",
    // 普通登录页
    LOGIN_PAGE_PATH: "/pages/account/phone-login",
    // 微信授权登录页
    WEIXIN_LOGIN_PAGE_PATH: "/pages/main/account/wx-intro",
    // 一些storage存储的常量KEY，不建议改，除非header里面的KEY_TOKEN可能会改成Authorization啥的
    KEY_TOKEN: 'TOKEN',
    KEY_LOGIN_USER_INFO: 'LOGIN_USER_INFO',
}

export default localCfg
