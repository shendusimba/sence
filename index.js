import * as message from './modules/message'
import * as normal from './modules/normal'
import * as request from './modules/request'
import * as router from './modules/router'
import * as storage from './modules/storage'
import * as wechat from './modules/wechat'
import loveU from 'love-u'
import localConfig from './modules/config'

export let _ = loveU

export default {
	/**
	 * config 是一些配置，比如首页路径，tabbar路径
	 * @param {*} Vue 
	 * @param {*} config 
	 */
	install(Vue, config = {}) {
		//将传入的配置和本地的配置进行合并
		_.assign(localConfig, config)

		//加入到全局Vue
		Vue.mixin({
			data() {
				return {
					...localConfig
				}
			},
			methods: {
				$SB: _,
				...message,
				...request,
				...router,
				...storage,
				...wechat,
				...normal
			}
		})
	}
}
