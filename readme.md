# sence
> 这省事，如你所愿。

专供uni-app项目使用
```shell
npm i sence
```

```javascript
//main.js

import sence from 'sence'

//将自己项目中的配置传进去，也可以多传，最终都会挂载到Vue实例上
Vue.use(sence,{
    // ajax路径前缀
    BASE_API:"http://localhost:8080"
    // 静态文件前缀(如阿里云CDN)
    STATIC_URL: "http://static.simbajs.com/",
    //提示信息驻留时间(毫秒)
    MSG_TIP_DURATION: 2000,
    //上传文件API(如七牛云)
    UPLOAD_URL: 'http://oss.qiniu.com/upload/',
    //tabbar页面路径数组，必填，防止go2时无法跳转
    TABBAR_PAGE_LIST: [],
    // 首页
    HOME_PAGE_PATH: "/pages/main/tabbar/index/index",
    // 普通登录页
    LOGIN_PAGE_PATH: "/pages/main/account/phone-login",
    // 微信授权登录页
    WEIXIN_LOGIN_PAGE_PATH: "/pages/main/account/wx-intro",
    // 一些storage存储的常量KEY，除了KEY_TOKEN用于放请求头鉴权按后台需要来改，其余不建议改
    KEY_TOKEN: 'TOKEN',
    KEY_LOGIN_USER_INFO: 'LOGIN_USER_INFO',
})
```